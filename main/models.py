from django.db import models
import json


class Article(models.Model):
    """
    Article model - Format of articles stored in db
    """
    author = models.TextField()
    article_content = models.TextField()

    def __str__(self):
        return self.author + " - " + self.article_content[:20]