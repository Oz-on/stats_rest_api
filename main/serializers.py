from rest_framework import serializers
from .models import Article

class ArticleSerializer(serializers.Serializer):
    author = serializers.CharField()
    article_content = serializers.CharField()

    def create(self, validated_data):
        """
        Create and return a new `Article` instance, given the validated data.
        """
        return ArticleSerializer.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Article` instance, given the validated data.
        """
        instance.author = validated_data.get('author', instance.author)
        instance.article_content = validated_data.get('article_content', instance.article_content)
        instance.save()
        return instance